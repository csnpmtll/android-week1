package com.boss

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import kotlin.random.Random

class MainActivity : AppCompatActivity() {


    lateinit var img: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        img = findViewById(R.id.imageView)
        val rollBtn : Button = findViewById(R.id.rollBtn)
        rollBtn.setOnClickListener{ rollDice() }
        val resetBtn : Button = findViewById(R.id.resetBtn)
        resetBtn.setOnClickListener { resetDice() }
    }
    private fun rollDice(){
        val randomNum:Int = ((Math.random()*6)+1).toInt()
        Log.d("number","${randomNum}")
        val image = when (randomNum){
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
        img.setImageResource(image)
        Toast.makeText(this,"button clicked",Toast.LENGTH_SHORT).show()
    }
    private fun resetDice(){
        img.setImageResource(R.drawable.empty_dice)
    }
}
